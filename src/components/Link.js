import React from 'react';
import { isLogin, LINKS_PER_PAGE } from '../constants';
import { timeDifferenceForDate } from '../utils';
import { useMutation } from '@apollo/client';
import { VOTE_MUTATION } from '../graphql/mutations';
import { FEED_QUERY } from '../graphql/queries';

import {
    Button,
    Grid,
    Typography,
} from '@material-ui/core';


const Link = ({ link, index }) => {

    const take = LINKS_PER_PAGE;
    const skip = 0;
    const orderBy = { createdAt: 'desc' };

    const [vote] = useMutation(VOTE_MUTATION, {
        variables: {
            linkId: link.id
        },
        refetchQueries: ['FeedQuery'],
        // update(cache, { data: { vote } }) {
        //     const { feed } = cache.readQuery({
        //         query: FEED_QUERY,
        //         variables: {
        //             take,
        //             skip,
        //             orderBy
        //         }
        //     });

        //     const updatedLinks = feed.links.map((feedLink) => {
        //         if (feedLink.id === link.id) {
        //             return {
        //                 ...feedLink,
        //                 votes: [...feedLink.votes, vote]
        //             };
        //         }
        //         return feedLink;
        //     });

        //     cache.writeQuery({
        //         query: FEED_QUERY,
        //         data: {
        //             feed: {
        //                 links: updatedLinks
        //             }
        //         },
        //         variables: {
        //             take,
        //             skip,
        //             orderBy
        //         }
        //     });
        // }
    });

    return (
        <Grid item xs={12}>
            <Grid container>
                <Grid item xs={1}>
                    <Typography>{index + 1}.</Typography>
                </Grid>
                {isLogin() && (
                    <Grid item xs={2}>
                        <Button
                            style={{ cursor: 'pointer' }}
                            onClick={vote}
                        >
                            ▲
                        </Button>
                    </Grid>
                )}
                <Grid item xs={9}>
                    <Typography>
                        {link.description} (<a href={link.url}>View</a>)
                    </Typography>
                    {isLogin() && (
                        <div className="f6 lh-copy gray">
                            {link.votes.length} votes | by{' '}
                            {link.postedBy ? link.postedBy.name : 'Unknown'}{' '}
                            {timeDifferenceForDate(link.createdAt)}
                        </div>
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
};
export default Link;