import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isLogin } from '../constants';

const PublicRoute = ({ component: Component, withHeader, restricted, ...rest }) => {
    return (
        <Route {...rest} render={props => (
            isLogin() && restricted ?
                <Redirect to="/new/:page" />
                : <Component {...props} />
        )} />
    );
};

export default PublicRoute;