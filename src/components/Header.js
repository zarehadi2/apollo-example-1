import React from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { attempLogout, isLogin } from '../constants';
import { makeStyles } from '@material-ui/core/styles';
import {
    AppBar,
    Toolbar,
    Typography,
    Button,
    Container,
} from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    tabtitle: {
        marginRight: theme.spacing(2),
        color: 'yellow',
        fontSize: '1.5rem',
        textAlign: 'center',
        userSelect: 'none',
    },
    title: {
        flexGrow: 1,
    },
    tabitem: {
        marginLeft: 5,
    },
    tablink: {
        color: '#fff',
        textDecoration: 'none',
    }
}));

const Header = () => {
    const classes = useStyles();

    const history = useHistory();

    return (
        <>
            <Container maxWidth="md">
                <AppBar position="static">
                    <Toolbar>
                        <Typography edge="start" className={classes.tabtitle} color="inherit" aria-label="menu">
                            new Links
                    </Typography>
                        <Typography variant="h6" className={classes.tabitem}>
                            <Link to="/" className={classes.tablink}> all </Link>
                        </Typography>
                        <div className={classes.tabitem}>|</div>
                        <Typography variant="h6" className={classes.tabitem}>
                            <Link to="/new/1" className={classes.tablink}> new </Link>
                        </Typography>
                        <div className={classes.tabitem}>|</div>
                        <Typography variant="h6" className={classes.tabitem}>
                            <Link to="/top" className={classes.tablink}> top </Link>
                        </Typography>
                        <div className={classes.tabitem}>|</div>
                        <Typography variant="h6" className={(!isLogin() ? classes.title + ' ' : '') + classes.tabitem}>
                            <Link to="/search" className={classes.tablink} > search </Link>
                        </Typography>
                        {isLogin() ? (
                            <>
                                <div className={classes.tabitem}>|</div>
                                <Typography variant="h6" className={classes.title + ' ' + classes.tabitem}>
                                    <Link to="/create" className={classes.tablink} > submit </Link>
                                </Typography>
                            </>
                        ) : ''}

                        {isLogin() ? (
                            <Button
                                color="inherit"
                                onClick={() => {
                                    attempLogout();
                                    history.push(`/`);
                                }}
                            >
                                logout
                            </Button>
                        ) : (
                                <Button
                                    color="inherit"
                                >
                                    <Link
                                        to="/login"
                                        className={classes.tablink}
                                    >
                                        login
                                </Link>
                                </Button>
                            )}
                    </Toolbar>
                </AppBar>
            </Container>
        </>
    );
};

export default Header;