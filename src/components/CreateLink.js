import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import { useHistory } from 'react-router';
import { LINKS_PER_PAGE } from '../constants';
import { CREATE_LINK_MUTATION } from '../graphql/mutations';
import { FEED_QUERY } from '../graphql/queries';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';


const CreateLink = () => {

    const [formState, setFormState] = useState({
        description: '',
        url: ''
    });

    const history = useHistory();

    const [createLink] = useMutation(CREATE_LINK_MUTATION, {
        variables: {
            description: formState.description,
            url: formState.url,
        },
        refetchQueries: ['FeedQuery'],
        // update: (cache, { data: { post } }) => {
        //     const take = LINKS_PER_PAGE;
        //     const skip = 0;
        //     const orderBy = { createdAt: 'desc' };

        //     const data = cache.readQuery({
        //         query: FEED_QUERY,
        //         variables: {
        //             take,
        //             skip,
        //             orderBy
        //         }
        //     });

        //     cache.writeQuery({
        //         query: FEED_QUERY,
        //         data: {
        //             feed: {
        //                 links: [post, ...data.feed.links]
        //             }
        //         },
        //         variables: {
        //             take,
        //             skip,
        //             orderBy
        //         }
        //     });
        // },
        onCompleted: () => history.push('/')
    });

    return (
        <>
            <Grid item xs={12}>
                <TextField fullWidth label="Description" size="small" variant="outlined"
                    value={formState.description}
                    onChange={(e) =>
                        setFormState({
                            ...formState,
                            description: e.target.value
                        })
                    }
                    type="text" />
            </Grid>
            <Grid item xs={12}>
                <TextField fullWidth label="Url" size="small" variant="outlined"
                    value={formState.url}
                    onChange={(e) =>
                        setFormState({
                            ...formState,
                            url: e.target.value
                        })
                    }
                    type="text" />
            </Grid>
            <Button color="primary" variant="contained"
                onClick={createLink}>
                Submit
                        </Button>
        </>
    );
};

export default CreateLink;