import React, { useState } from 'react';
import { useLazyQuery } from '@apollo/client';
import { Link } from './';
import { FEED_SEARCH_QUERY } from '../graphql/queries';
import {
    Button,
    Grid,
    TextField,
} from '@material-ui/core';


const Search = () => {

    const [searchFilter, setSearchFilter] = useState('');
    const [executeSearch, { data }] = useLazyQuery(
        FEED_SEARCH_QUERY
    );

    return (
        <>
            <Grid item xs={10}>
                <TextField fullWidth label="Search" size="small" variant="outlined"
                    type="text" onChange={(e) => setSearchFilter(e.target.value)} />
            </Grid>
            <Grid item xs={2}>
                <Button onClick={() =>
                    executeSearch({
                        variables: { filter: searchFilter }
                    })}
                >
                    OK
                            </Button>
            </Grid>

            {data ? (
                data.feed.links.map((link, index) => (
                    <Link key={link.id} link={link} index={index} />
                ))
            ) : null
            }
        </>
    );
};

export default Search;