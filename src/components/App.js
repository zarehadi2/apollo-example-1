import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import {
  Container,
  Grid,
  CssBaseline
} from '@material-ui/core';

import {
  Header,
  Login,
  Register,
  Search,
  PrivateRoute,
  PublicRoute,
  LinkList,
  CreateLink
} from './';

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(3),
  },
}));

const Main = ({ children }) => {
  const classes = useStyles();
  console.log(children);
  return (
    <>
      <CssBaseline />
      <Container fixed>
        <Header />
        <Container className={classes.container} maxWidth="xs">
          <Grid container direction="row" justify="center" alignItems="center" spacing={3}>
            {children}
          </Grid>
        </Container>
      </Container>
    </>
  );
}

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <PublicRoute path="/login" restricted={true} component={Login} />
        <PublicRoute path="/register" restricted={true} component={Register} />

        {/* <Route path='/admin/:path?' exact>
          <MainAdmin>
            <Switch>
              <Route path='/admin' exact component={Dashboard} />
              <Route path='/admin/setting' component={Setting} />
            </Switch>
          </MainAdmin>
        </Route>
         */}

        <Route>
          <Main>
            <Switch>
              <PublicRoute path={`/`} restricted={false} exact component={LinkList} />

              <PrivateRoute path={`/create`} component={CreateLink} />

              <PublicRoute path={`/search`} restricted={false} component={Search} />
              <PublicRoute path={`/top`} restricted={false} component={LinkList} />
              <PublicRoute path={`/new/:page`} restricted={false} component={LinkList} />
            </Switch>
          </Main>
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

export default App;
