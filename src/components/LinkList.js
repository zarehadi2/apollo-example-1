import React from 'react';
import { useHistory } from 'react-router';
import { Link } from './';
import { useQuery } from '@apollo/client';
import { LINKS_PER_PAGE } from '../constants';
import { NEW_LINKS_SUBSCRIPTION, NEW_VOTES_SUBSCRIPTION } from '../graphql/subscriptions';
import { FEED_QUERY } from '../graphql/queries';
import { Button, Grid } from '@material-ui/core';


const getQueryVariables = (isNewPage, page) => {
  const skip = isNewPage ? (page - 1) * LINKS_PER_PAGE : 0;
  const take = isNewPage ? LINKS_PER_PAGE : 100;
  const orderBy = { createdAt: 'desc' };
  return { take, skip, orderBy };
};

const getLinksToRender = (isNewPage, data) => {
  if (isNewPage) {
    return data.feed.links;
  }
  const rankedLinks = data.feed.links.slice();
  rankedLinks.sort(
    (l1, l2) => l2.votes.length - l1.votes.length
  );
  return rankedLinks;
};

const LinkList = () => {
  const history = useHistory();
  const isNewPage = history.location.pathname.includes(
    'new'
  );
  const pageIndexParams = history.location.pathname.split(
    '/'
  );
  const page = parseInt(
    pageIndexParams[pageIndexParams.length - 1]
  );
  const pageIndex = page ? (page - 1) * LINKS_PER_PAGE : 0;

  const {
    data,
    loading,
    error,
    subscribeToMore
  } = useQuery(FEED_QUERY, {
    variables: getQueryVariables(isNewPage, page)
  });

  subscribeToMore({
    document: NEW_LINKS_SUBSCRIPTION,
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData.data) return prev;
      const newLink = subscriptionData.data.newLink;
      const exists = prev.feed.links.find(
        ({ id }) => id === newLink.id
      );
      if (exists) return prev;

      return Object.assign({}, prev, {
        feed: {
          links: [newLink, ...prev.feed.links],
          count: prev.feed.links.length + 1,
          __typename: prev.feed.__typename
        }
      });
    }
  });

  subscribeToMore({
    document: NEW_VOTES_SUBSCRIPTION
  });

  if (loading) {
    return <p>Loading...</p>;
  }
  if (error) {
    return <pre>{JSON.stringify(error, null, 2)}</pre>;
  }

  return (
    <>
      {data ? (
        <>
          {getLinksToRender(isNewPage, data).map(
            (link, index) => (
              <Link
                key={link.id}
                link={link}
                index={index + pageIndex}
              />
            )
          )}
          {isNewPage && (
            <Grid item xs={12} className="gray">
              <Grid container>
                <Grid item xs={6}>
                  <Button color="primary" fullWidth variant="contained"
                    onClick={() => {
                      if (page > 1) {
                        history.push(`/new/${page - 1}`);
                      }
                    }}>
                    Previous
                        </Button>
                </Grid>
                <Grid item xs={6}>
                  <Button color="primary" fullWidth variant="contained"
                    onClick={() => {
                      if (
                        page <=
                        data.feed.count / LINKS_PER_PAGE
                      ) {
                        const nextPage = page + 1;
                        history.push(`/new/${nextPage}`);
                      }
                    }}>
                    Next
                      </Button>
                </Grid>
              </Grid>
            </Grid>
          )}
        </>
      ) : null}
    </>
  );
};

export default LinkList;