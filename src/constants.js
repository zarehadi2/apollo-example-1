const AUTH_TOKEN = 'auth-token';
export const LINKS_PER_PAGE = 5;


export const attempLogin = (token) => {
    localStorage.setItem(AUTH_TOKEN, token);
}

export const attempLogout = () => {
    localStorage.removeItem(AUTH_TOKEN);
}

export const isLogin = () => {
    if (localStorage.getItem(AUTH_TOKEN)) {
        return true;
    }

    return false;
}

export const getToken = () => {
    return localStorage.getItem(AUTH_TOKEN)
}